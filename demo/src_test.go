/*
 * @Author: huangxx
 * @Date: 2021-07-22 10:25:49
 * @Note:
 * @LastEditTime: 2021-07-22 10:25:51
 */
// filename: src_test.go
package demo

import "testing"

func TestAdd1(t *testing.T) {
	if Add(2, 3) != 5 {
		t.Error("result is wrong!")
	} else {
		t.Log("result is right")
	}
}
